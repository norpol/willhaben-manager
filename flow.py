#!/usr/bin/env python3
from types import SimpleNamespace
import requests
import json
import bs4
from subprocess import check_output
from urllib.parse import urlparse, urlunparse, parse_qs
import logging
from time import sleep
from math import ceil

#import http.client as http_client
#http_client.HTTPConnection.debuglevel = 1

# You must initialize logging, otherwise you'll not see debug output.
#logging.basicConfig()
#logging.getLogger().setLevel(logging.DEBUG)
#requests_log = logging.getLogger("requests.packages.urllib3")
#requests_log.setLevel(logging.DEBUG)
#requests_log.propagate = True



class SSOAuthenticate:
    def __init__(self):
        self.session = requests.Session()
        self.api_url = "https://sso.willhaben.at/auth/realms/willhaben"
        self.headers = {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/87.0",
                }
        self.session.headers = self.headers


    def login(self):
        self.session.cookies = requests.cookies.RequestsCookieJar()

        url = "https://www.willhaben.at/iad/myprofile/login"

        r = self.session.get(url, allow_redirects=True)

        soup = bs4.BeautifulSoup(r.content, features="lxml")
        form = soup.find(id="kc-form-login")
        url = form.attrs.get("action")

        data = {
                "username": check_output(["pass", "entity/tiny/wolfgang/willhaben.at/mail"]).decode().strip(),
                "password": check_output(["pass", "entity/tiny/wolfgang/willhaben.at/password"]).decode().strip(),
                "rememberMe": "on",
                }

        r = self.session.post(url, data=data, allow_redirects=True)

        return r


class Adverts:
    def __init__(self, session):
        self.session = session
        self.session.headers = {
                    "Accept": "application/json",
                    "X-WH-Client": "api@willhaben.at;responsive_web;server;1.0.0;desktop",
                    "x-bbx-csrf-token": self.csrf_token,
                    "Sec-Fetch-Dest": "empty",
                    "Sec-Fetch-Mode": "cors",
                    "Sec-Fetch-Site": "same-origin",
                    "TE": "Trailers",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0",
                    "Referer": "https://www.willhaben.at/iad/myprofile/myadverts?page=15",
                }
        self.api_url = "https://www.willhaben.at"
        self.adverts = []
        self.login_id = self.api_get("webapi/iad/user/statistics").json().get("loginId")
        if self.login_id < 0:
            print("error couldn't retrieve user authentication")
        #self.api_url = "http://localhost/anything"


    def adverts_from_user(self, page=None, amount=None, recurse=True):
        page = page or 1
        amount = amount or 100
        url = f"https://www.willhaben.at/webapi/iad/atverz/showpagedadsfromuser?page={page}&rows={amount}&vertical=estate&vertical=motor&vertical=bap"

        r = self.session.get(url)
        c = r.json(object_hook=lambda d: SimpleNamespace(**d))

        total = ceil(c.totalNumberOfAdvertDetails/amount)

        self.adverts.extend(c.verticalList[2].advertDetails)

        if recurse:
            for i in range(page + 1, total + 1):
                self.adverts_from_user(page=i, amount=amount, recurse=False)


    @property
    def csrf_token(self):
        return self.session.cookies.get("x-bbx-csrf-token", domain="www.willhaben.at")

    @property
    def ad_types(self):
        ret = []

        for ad in self.adverts:
            ret.append(ad.advertEditStatusActionsList.statusName)

        return tuple(set(ret))

    def find_ads(self, status=None):
        ret = []
        for i, ad in enumerate(self.adverts):
            if ad.advertEditStatusActionsList.statusName == status:
                ret.append(tuple((i, ad.id)))
        return tuple(ret)


    def delete_advert(self, id):
        self.session.headers.update({"x-bbx-csrf-token": self.csrf_token})
        url = f"https://www.willhaben.at/webapi/iad/bap/{id}?loginid={self.login_id}"
        r = self.session.delete(url)
        r.raise_for_status()
        return r


    @property
    def sold_adverts(self):
        return self.find_ads(status="verkauft")


    @property
    def expired_adverts(self):
        return self.find_ads(status="abgelaufen")


    @property
    def waiting_adverts(self):
        return self.find_ads(status="Bearbeitung")

    @property
    def inactive_adverts(self):
        return self.find_ads(status="inaktiv")

    @property
    def active_adverts(self):
        self.find_ads(status="aktiv")


    def url(self, path):
        url = f"{self.api_url}/{path}"
        return url

    def api_post(self, path=None, data=None, params=None):
        r = self.session.post(self.url(path), data=data, params=params, allow_redirects=True)
        r.raise_for_status()
        return r

    def api_get(self, path=None, params=None, data=None):
        self.session.headers.update({"x-bbx-csrf-token": self.session.cookies.get("x-bbx-csrf-token")}),
        r = self.session.get(self.url(path), params=params, allow_redirects=True)
        r.raise_for_status()
        return r



class Advert(Adverts):
    def __init__(self, cookies=None, init_id=None):
        self.init_id = init_id or None
        self.api_url = "https://www.willhaben.at"
        self.session = requests.Session()
        self.session.cookies = cookies.copy()
        #self.session.cookies = requests.cookies.cookiejar_from_dict(cookies)
        self.session.headers = {
                    #"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
                    "Accept": "application/json,text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "de-AT,de;q=0.5",
                    "X-WH-Client": "api@willhaben.at;responsive_web;server;1.0.0;desktop",
                    "x-bbx-csrf-token": self.csrf_token,
                    "Sec-Fetch-Dest": "document",
                    "Sec-Fetch-Mode": "cors",
                    "Sec-Fetch-Site": "same-origin",
                    "TE": "Trailers",
                    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0",
                    "Referer": "https://www.willhaben.at/iad/myprofile/myadverts?page=15",
                }
        print(self.api_get("webapi/iad/user/statistics").json())


    def api_post(self, path=None, data=None, params=None):
        self.session.headers.update({"x-bbx-csrf-token": self.csrf_token})
        r = self.session.post(self.url(path), data=data, params=params, allow_redirects=True)
        print(path, params, "POST", "redirected to:", r.url)
        r.raise_for_status()
        return r


    def api_get(self, path=None, params=None):
        self.session.headers.update({"x-bbx-csrf-token": self.csrf_token})
        r = self.session.get(self.url(path), params=params, allow_redirects=True)
        print(path, params, "GET", "redirected to:", r.url)
        r.raise_for_status()
        return r


    def duplicate(self, init_id=None):
        init_id = init_id or self.init_id
        r = self.api_post("webapi/iad/bap/copy/" + str(init_id))
        c = r.json(object_hook=lambda d: SimpleNamespace(**d))
        return c

    def create_ad(self, ad=None):
        params = {
                "adId": ad.adId,
                "adTypeId": ad.adTypeId,
                "newProductId": ad.productId,
                "userAction": "finish",
        }


        # should redirect to execution e2s1 and
        # set-cookie: SRV=<digit>|<string>; path=/; HttpOnly
        r = self.api_get("iad/adinput/bap", params=params)
        url = urlparse(r.url)
        # execution comes from SRV=1|<something>, whereas 1 == e1s1...
        self.current_execution = parse_qs(url.query).get("execution")[0]
        return r


    def execution(self, num=None):
        ex = self.current_execution[1:2]
        print(type(ex))
        exec_string = f"e{ex}s{num}"
        print(exec_string)
        return {"execution": exec_string}


    def e2s1(self, ad=None):
        try:
            suggestionTreeId = ad.taggingData.tmsDataValues.tmsData.category_level_id_3
        except AttributeError:
            suggestionTreeId = ad.taggingData.tmsDataValues.tmsData.category_level_id_2
            print("category_level_3 missing, picking 2", suggestionTreeId)

        data = {
                "heading": ad.heading,
                "price": str(float(ad.price)).replace(".", ","),
                "categorySuggestion": 0,
                "categoryTreeId": ad.categoryTreeId,
                "showPhoneNumberInAd": str(not ad.hidePhoneNo).lower(),
                "_showPhoneNumberInAd": "no" if ad.hidePhoneNo else "on",
                "Selbstabholung": ad.treeAttributes[1].selectedValueTreeIds[0],
                "Versand": ad.treeAttributes[1].selectedValueTreeIds[0],
                "description": ad.description,
                "contactFullName": f"{ad.firstName or ''} {ad.surName or ''}".strip(),
                "contactPhone": ad.phoneNo,
                "contactPhone2": ad.phoneNo2 or "",
                "contactEmail": ad.emailAddress,
                "adAddressBean.address1": ad.street,
                "adAddressBean.countryId": ad.countryId,
                "adAddressBean.addressPostcode": ad.postCode,
                "adAddressBean.locationId": ad.locationId,
                "adAddressBean.previousPostcode": "",
                "address2": ad.taggingData.tmsDataValues.tmsData.region_level_2,
                "showPointOfSale": str(not ad.hidePointOfSale).lower(),
                "_showPointOfSale": "no" if ad.hidePointOfSale else "on",
                "suggestionsCount": 3, # TODO ???
                "suggestionTreeId": suggestionTreeId,
                "treeAttributeIds": ",".join([str(int) for int in ad.treeAttributeIds]),
                "position": 0,
                "isCategorySwitch": "false", # TODO ???
                "_eventId_proceed": ""
            }

        headers = self.session.headers
        headers.update({'content-type':'application/x-www-form-urlencoded; charset=UTF-8'})
        r = self.session.post(self.url("iad/adinput/bap"), params=self.execution(1), data=data, headers=headers)
        print(r.url, r.encoding)
        return r

    def adzimage(self):
        """ accessing this endpoint at step 2 (image upload)
        nothing implys that images are passed over"""
        # https://www.willhaben.at/iad/ajax/adzimage/441101960



    def e2s2(self):
        r = self.api_post("iad/adinput/bap", params=self.execution(2), data="files%5B%5D=&_eventId_proceed=")

    def e2s3(self):
        """shipment options"""
        data = {
            "currentDeliveryOption": "",
            "currentDeliveryCarrier": "",
            "deliveryCarrier": "unselected",
            "deliveryOption": "optout",
            "deliveryOptionOptout": "on",
            "_eventId_proceed": "",
        }

        r = self.api_post("iad/adinput/bap", params=self.execution(3), data=data)


    def e2s4(self):
        """advertise your product"""

        # productGroup ID's keep changing
        data = {
            "productGroup-0": 3112,
            "_upsellingProductGroups[0].upsellingProducts[0].selected": "on",
            "_upsellingProductGroups[0].upsellingProducts[1].selected": "on",
            "productGroup-1": 3212,
            "_upsellingProductGroups[1].upsellingProducts[0].selected": "on",
            "_upsellingProductGroups[1].upsellingProducts[1].selected": "on",
            "productGroup-2": 32003,
            "_upsellingProductGroups[2].upsellingProducts[0].selected": "on",
            "productGroup-3": 33003,
            "_upsellingProductGroups[3].upsellingProducts[0].selected": "on",
            "_eventId_proceed": "",
            }
        r = self.api_post("iad/adinput/bap", params=self.execution(4), data=data)
        # redirects to e2s5


    def e2s5(self):
        """finalize"""
        r = self.api_post("iad/adinput/bap", data={"_eventId_payAndPublish": ""}, params=self.execution(5))
        return r
        # redirects to https://www.willhaben.at/iad/adinput/bap/finished?adId=441101960&error=false

    def extend(self, init_id=None):
        ad = self.duplicate(init_id)
        self.ad = ad
        print(ad.heading, ad.price)
        self.create_ad(ad)
        self.e2s1(ad)
        self.e2s2()
        self.e2s3()
        self.e2s4()
        return self.e2s5()

